#ifndef PARAMETROS_H_INCLUDED
#define PARAMETROS_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>

#define CROSSOVER 0.8
#define MUTACAO 0.03
#define GERACAO 5000
#define POPULACAO 500

typedef struct _parametros_{
    int qtdCarros;
    int qtdFerrys;
    int *satisfacaoCarro;
    int **pesoCarro;
    int *capacidadeFerry;
    int *tempoChegadaCarro;
    int *tempoSaidaFerry;
    int otimo;
    float taxa_mutacao;
    float taxa_crossover;
    int tam_pop;
    int numero_iteracoes;
}PARAMETROS;

PARAMETROS *inicializa_parametros(char *instancia);

void destroi_parametros(PARAMETROS *param);

#endif // PARAMETROS_H_INCLUDED
