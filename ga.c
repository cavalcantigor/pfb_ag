#include "ga.h"

int *ga(PARAMETROS *param){
    INDIVIDUO *pop = NULL, *nova_pop = NULL;
    int pai, mae, **filhos = NULL;

    int i, j, k, qtdNovosIndividuos, pontoSubstituicao, iteracoes = 0;

    int *best = NULL;

    srand(time(NULL)); // Para utilizar em eventuais chamadas a rand

    best = (int *)malloc(param->qtdFerrys * sizeof(int));

    if(best == NULL){
        printf("Erro ao alocar memoria!\nFuncao >> ga\nAlocando >> best\n");
        system("pause");
        return NULL;
    }
    system("cls");
    printf("\n\nConstruindo populacao inicial...\n");

    pop = constroi_populacao(param); // OK

    if(pop != NULL){
        printf("\nPopulacao inicial gerada.\nTamanho da populacao >> %d\n", param->tam_pop);
        printf("\n");
    }else{
        printf("Erro ao construir populacao inicial!\n");
        system("pause");
        return NULL;
    }

    for(i = 0; i < param->tam_pop; i++){
        pop[i].fitness = funcao_objetivo(param, pop[i].cromossomo); // OK
    }

    for(j = 0; j < param->qtdCarros; j++){
        printf("%d ", pop[0].cromossomo[j]);
    }
    printf(" Fitness: %d\n", pop[0].fitness);
    system("pause");

    while(iteracoes < param->numero_iteracoes){
        qtdNovosIndividuos = 0;
        nova_pop = (INDIVIDUO *)malloc((int)(param->tam_pop * 0.3) * sizeof(INDIVIDUO));
        if(nova_pop == NULL){
            printf("Erro ao alocar memoria!\nFuncao >> ga\nAlocando >> nova_pop\n");
            system("pause");
            return NULL;
        }
        for(i = 0; i < (int)(param->tam_pop * 0.3); i++){
            nova_pop[i].cromossomo = (int *)malloc(param->qtdCarros * sizeof(int));
            if(nova_pop[i].cromossomo == NULL){
                printf("Erro ao alocar memoria!\nFuncao >> ga\nAlocando >> nova_pop[i]\n");
                system("pause");
                return NULL;
            }
        }

        qsort((void *)pop, param->tam_pop, sizeof(INDIVIDUO), cmpfunc); // OK - ordem crescente

        for(i = 0; i < (int)(param->tam_pop * 0.15); i++){
            if(((float)(rand() % 1000)/1000) <= param->taxa_crossover){

                pai = roleta(param, pop, 0);
                mae = roleta(param, pop, pai);

                filhos = crossover_pmx(param, pop[pai].cromossomo, pop[mae].cromossomo); // OK

                if(filhos == NULL){
                    printf("Erro ao alocar memoria!\n Funcao >> ga\nAlocando >> filhos\n");
                    system("pause");
                    return NULL;
                }

                for(j = 0; j < param->qtdCarros; j++){
                    nova_pop[qtdNovosIndividuos].cromossomo[j] = filhos[0][j];
                }
                mutacao(param, nova_pop[qtdNovosIndividuos].cromossomo);
                nova_pop[qtdNovosIndividuos].fitness = funcao_objetivo(param, nova_pop[qtdNovosIndividuos].cromossomo); // OK

                for(j = 0; j < param->qtdCarros; j++){
                    nova_pop[qtdNovosIndividuos + 1].cromossomo[j] = filhos[1][j];
                }
                mutacao(param, nova_pop[qtdNovosIndividuos + 1].cromossomo);
                nova_pop[qtdNovosIndividuos + 1].fitness = funcao_objetivo(param, nova_pop[qtdNovosIndividuos + 1].cromossomo); // OK

                qtdNovosIndividuos = qtdNovosIndividuos + 2;

                if(filhos != NULL){
                    if(filhos[0] != NULL){
                        free(filhos[0]);
                    }
                    if(filhos[1] != NULL){
                        free(filhos[1]);
                    }
                    free(filhos);
                }
            }
        }

        qtdNovosIndividuos -= 2;

        qsort((void *)nova_pop, qtdNovosIndividuos, sizeof(INDIVIDUO), cmpfunc);

        pontoSubstituicao = (rand() % ((int)(param->tam_pop * 0.8) - qtdNovosIndividuos));

        k = 0;
        for(i = pontoSubstituicao; i < (pontoSubstituicao + qtdNovosIndividuos); i++){
            for(j = 0; j < param->qtdCarros; j++){
                pop[i].cromossomo[j] = nova_pop[k].cromossomo[j];
            }
            pop[i].fitness = nova_pop[k].fitness;
            k++;
        }

        destruir_populacao(param, nova_pop, qtdNovosIndividuos);

        iteracoes++;

    }

    printf("\n\nMelhor individuo: \n");
    for(j = 0; j < param->qtdCarros; j++){
        printf("%d ", pop[param->tam_pop-1].cromossomo[j]);
    }
    printf(" Fitness: %d\n", pop[param->tam_pop-1].fitness);

    system("pause");

    destruir_populacao(param, pop, 0);

    return best;
}

int cmpfunc(const void *a, const void *b){
    INDIVIDUO *indA, *indB;

    indA = (INDIVIDUO *)a;
    indB = (INDIVIDUO *)b;

    return (indA->fitness - indB->fitness);
}

INDIVIDUO *constroi_populacao(PARAMETROS *param){
	INDIVIDUO *pop = NULL;

    int capacidades[param->qtdFerrys];
    int escolhido;

	int i, j, k;

	pop = (INDIVIDUO *)malloc(param->tam_pop * sizeof(INDIVIDUO));

	if(pop == NULL){
		printf("Erro ao alocar memoria!\nFuncao >> constroi_populacao\nAlocando >> pop\n");
        system("pause");
        return NULL;
	}

    for(i = 0; i < param->tam_pop; i++){
        pop[i].cromossomo = (int *)malloc(param->qtdCarros * sizeof(int));
        if(pop[i].cromossomo == NULL){
            printf("Erro ao alocar memoria!\nFuncao >> constroi_populacao\nAlocando >> pop[i]\n");
            system("pause");
            return NULL;
        }
    }

    i = 0;

        for(k = 0; k < param->qtdFerrys; k++){
            capacidades[k] = param->capacidadeFerry[k];
        }
        for(j = 0; j < param->qtdCarros; j++){
            escolhido = -1;
            for(k = 0; k < param->qtdFerrys; k++){
                if((param->tempoSaidaFerry[k] - param->tempoChegadaCarro[j]) >= 0){
                    if((capacidades[k] - param->pesoCarro[k][j]) > 0){
                        capacidades[k] -= param->pesoCarro[k][j];
                        escolhido = k;
                        break;
                    }
                }
            }

            pop[i].cromossomo[j] = (escolhido >= 0) ? (escolhido + 1) : 0;

        }
        i++;


    for(i = 1; i < param->tam_pop; i++){
        for(j = 0; j < param->qtdCarros; j++){
            pop[i].cromossomo[j] = rand() % (param->qtdFerrys + 1);
        }
    }

	return pop;
}

int funcao_objetivo(PARAMETROS *param, int *candidato){
    int i;
    int fitness = 0, *capacidades;

    for(i = 0; i < param->qtdCarros; i++){
        fitness += ((candidato[i] == 0) ? 0 : param->satisfacaoCarro[i]);
    }

    capacidades = (int *)malloc(param->qtdFerrys * sizeof(int));
    if(capacidades == NULL){
        printf("\nErro ao alocar memoria!\nFuncao >> funcao_objetivo\nAlocando >> capacidades\n");
        system("pause");
        return 0;
    }
    for(i = 0; i < param->qtdFerrys; i++){
        capacidades[i] = param->capacidadeFerry[i];
    }

    for(i = 0; i < param->qtdCarros; i++){
        if((candidato[i]-1) >= 0){
            capacidades[candidato[i]-1] -= param->pesoCarro[candidato[i]-1][i];
            if(capacidades[candidato[i]-1] < 0 || ((param->tempoSaidaFerry[candidato[i]-1] - param->tempoChegadaCarro[i]) < 0)){
                //fitness -= ((1000 * param->qtdCarros)/(i+1));
                fitness = 10;
                break;
            }
        }
    }

    return fitness;
}

/* Esta rotina devolve o individuo escolhido pelo mecanismo da roleta russa */
/* Implementação encontrada em: http://www.decom.ufop.br/marcone/Disciplinas/InteligenciaComputacional/InteligenciaComputacional.htm */
int roleta(PARAMETROS *param, INDIVIDUO *pop, int anterior){
    int j;
    double aux;
    double *escala, *fracao;
    int escolhido;

    fracao = cria_vetor_double(param);
    escala = cria_vetor_double(param);

    int soma = 0;
    for (j = 0; j < param->tam_pop; j++){
        soma += pop[j].fitness;
        //printf("cromossomo[%d] = %d \n", j, pop[j].fitness);
    }
    for (j = 0; j < param->tam_pop; j++){
        fracao[j] = (double) pop[j].fitness/soma;
        //printf("fracao[%d] = %f \n", j, fracao[j]);
    }
    //system("cls");
    escala[0] = fracao[0];
    for (j = 1; j < param->tam_pop; j++){
        escala[j] = escala[j-1] + fracao[j];
        //printf("escala[%d] = %f \n", j, escala[j]);
    }
    //system("pause");

    escolhido = anterior;
    while(escolhido == anterior){
        aux = (float)(rand() % 1000)/1000;
        j = 0;
        //printf("\n\naux: %f\n\n", aux);
        while (escala[j] <= aux) j++;
        escolhido = j;
        //system("pause");
    }

    free(fracao);
    free(escala);

    return escolhido;
}

/* cria memoria para um vetor de tam posicoes com elementos do tipo double */
/* Implementação encontrada em: http://www.decom.ufop.br/marcone/Disciplinas/InteligenciaComputacional/InteligenciaComputacional.htm */
double *cria_vetor_double(PARAMETROS *param){
    double *vetor;

    vetor = (double *) malloc(param->tam_pop * sizeof(double));
    if (!vetor){
        printf("Falta memoria para alocar o vetor de ponteiros\n");
        exit(1);
    }
    return vetor;
}

int **crossover(PARAMETROS *param, int *pai, int *mae){
    int **filhos = NULL;
    int i, pontoCorte;

    filhos = (int **)malloc(2 * sizeof(int *));

    if(filhos == NULL){
        printf("Erro ao alocar memoria!\nFuncao >> crossover\nAlocando >> filho_1 e filho_2\n");
        system("pause");
        return NULL;
    }

    filhos[0] = (int *)malloc(param->qtdCarros * sizeof(int));
    filhos[1] = (int *)malloc(param->qtdCarros * sizeof(int));

    if(filhos[0] == NULL || filhos[1] == NULL){
        printf("Erro ao alocar memoria!\nFuncao >> crossover\nAlocando >> filho_1 e filho_2\n");
        system("pause");
        return NULL;
    }

    pontoCorte = (rand() % (param->qtdCarros - 1)) + 1;

    for(i = 0; i < pontoCorte; i++){
        filhos[0][i] = pai[i];
        filhos[1][i] = mae[i];
    }
    for(i = pontoCorte; i < param->qtdCarros; i++){
        filhos[0][i] = mae[i];
        filhos[1][i] = pai[i];
    }

    return filhos;
}

int **crossover_pmx(PARAMETROS *param, int *pai, int *mae){
    int **filhos = NULL;
    int i, pontoCorte_1, pontoCorte_2;

    filhos = (int **)malloc(2 * sizeof(int *));

    if(filhos == NULL){
        printf("Erro ao alocar memoria!\nFuncao >> crossover\nAlocando >> filho_1 e filho_2\n");
        system("pause");
        return NULL;
    }

    filhos[0] = (int *)malloc(param->qtdCarros * sizeof(int));
    filhos[1] = (int *)malloc(param->qtdCarros * sizeof(int));

    if(filhos[0] == NULL || filhos[1] == NULL){
        printf("Erro ao alocar memoria!\nFuncao >> crossover_pmx\nAlocando >> filho_1 e filho_2\n");
        system("pause");
        return NULL;
    }

    pontoCorte_1 = 0;
    pontoCorte_2 = 0;
    while(!(pontoCorte_1 < pontoCorte_2)){
        pontoCorte_1 = (rand() % (param->qtdCarros - 1)) + 1;
        pontoCorte_2 = (rand() % (param->qtdCarros - 1)) + 1;
    }

    for(i = 0; i < param->qtdCarros; i++){
        filhos[0][i] = pai[i];
        filhos[1][i] = mae[i];
    }

    for(i = pontoCorte_1; i < pontoCorte_2; i++){
        filhos[0][i] = pai[i];
        filhos[1][i] = mae[i];
    }

    return filhos;
}

void mutacao(PARAMETROS *param, int *candidato){
    int i;
    for(i = 0; i < param->qtdCarros; i++){
        if(((float)(rand() % 1000)/1000) < param->taxa_mutacao){
            candidato[i] = rand() % (param->qtdFerrys + 1);
        }
    }
}

void destruir_populacao(PARAMETROS *param, INDIVIDUO *pop, int tam){
    int i;
    if(pop != NULL){
        if(tam == 0){
            for(i = 0; i < param->tam_pop; i++){
                if(pop[i].cromossomo != NULL){
                    free(pop[i].cromossomo);
                }
            }
        }else{
            for(i = 0; i < tam; i++){
                if(pop[i].cromossomo != NULL){
                    free(pop[i].cromossomo);
                }
            }
        }
        free(pop);
    }
}

