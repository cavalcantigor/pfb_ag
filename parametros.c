#include "parametros.h"

PARAMETROS *inicializa_parametros(char *instancia){
    PARAMETROS *param = NULL;
    FILE *arq = NULL;

    int i = 0, j = 0;

    param = (PARAMETROS *)malloc(sizeof(PARAMETROS));

    if(param == NULL){
        printf("Erro ao alocar memoria!\n");
        system("pause");
        exit(1);
    }

    param->capacidadeFerry = NULL;
    param->pesoCarro = NULL;
    param->satisfacaoCarro = NULL;
    param->qtdCarros = 0;
    param->qtdFerrys = 0;
    param->otimo = 0.f;
    param->tempoSaidaFerry = NULL;
    param->tempoChegadaCarro = NULL;

    arq = fopen(instancia, "r");
    printf("%s", instancia);
    if(arq == NULL){
        printf("Erro ao abrir arquivo de instancia!\n");
        system("pause");
        exit(1);
    }

    fscanf(arq, "%d %d %d", &param->qtdCarros, &param->qtdFerrys, &param->otimo);

    param->satisfacaoCarro = (int *)malloc(param->qtdCarros * sizeof(int));

    if(param->satisfacaoCarro == NULL){
        printf("Erro ao alocar memoria!\n");
        system("pause");
        exit(1);
    }

    while(i < param->qtdCarros){
        fscanf(arq, "%d", &param->satisfacaoCarro[i]);
        i++;
    }

    param->pesoCarro = (int **)malloc(param->qtdFerrys * sizeof(int *));

    for(i = 0; i < param->qtdFerrys; i++){
        param->pesoCarro[i] = (int *)malloc(param->qtdCarros * sizeof(int));
        if(param->pesoCarro == NULL){
            printf("Erro ao abrir arquivo de instancia!\n");
            system("pause");
            exit(1);
        }
    }

    i = 0;
    while(i < param->qtdFerrys){
        while(j < param->qtdCarros){
            fscanf(arq, "%d ", &param->pesoCarro[i][j]);
            j++;
        }
        j = 0;
        i++;
    }

    param->capacidadeFerry = (int *)malloc(param->qtdFerrys * sizeof(int));

    if(param->capacidadeFerry == NULL){
        printf("Erro ao abrir arquivo de instancia!\n");
        system("pause");
        exit(1);
    }

    param->tempoChegadaCarro = (int *)malloc(param->qtdCarros * sizeof(int));

    if(param->tempoChegadaCarro == NULL){
        printf("Erro ao abrir arquivo de instancia!\n");
        system("pause");
        exit(1);
    }

    i = 0;
    while(i < param->qtdCarros){
        fscanf(arq, "%d", &param->tempoChegadaCarro[i]);
        i++;
    }

    param->tempoSaidaFerry = (int *)malloc(param->qtdFerrys * sizeof(int));

    if(param->tempoSaidaFerry == NULL){
        printf("Erro ao abrir arquivo de instancia!\n");
        system("pause");
        exit(1);
    }

    i = 0;
    while(i < param->qtdFerrys){
        fscanf(arq, "%d", &param->tempoSaidaFerry[i]);
        i++;
    }


    i = 0;
    while(i < param->qtdFerrys){
        fscanf(arq, "%d", &param->capacidadeFerry[i]);
        i++;
    }

    return param;
}

void destroi_parametros(PARAMETROS *param){
    int i = 0;
    if(param->capacidadeFerry != NULL)
        free(param->capacidadeFerry);
    if(param->satisfacaoCarro != NULL)
        free(param->satisfacaoCarro);
    if(param->pesoCarro != NULL){
        for(i = 0; i < param->qtdFerrys; i++){
            if(param->pesoCarro[i] != NULL)
                free(param->pesoCarro[i]);
        }
        free(param->pesoCarro);
    }
    if(param->tempoChegadaCarro != NULL)
        free(param->tempoChegadaCarro);
    if(param->tempoSaidaFerry != NULL)
        free(param->tempoSaidaFerry);
    param->otimo = 0.f;
    param->qtdCarros = 0;
    param->qtdFerrys = 0;
    param->taxa_crossover = 0.f;
    param->taxa_mutacao = 0.f;
    param->tam_pop = 0;
    param->numero_iteracoes = 0;

    free(param);
}


