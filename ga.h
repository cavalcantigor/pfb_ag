#ifndef GA_H_INCLUDED
#define GA_H_INCLUDED

#include "parametros.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct _individuo_{
    int *cromossomo;
    int fitness;
}INDIVIDUO;

int *ga(PARAMETROS *param);
INDIVIDUO *constroi_populacao(PARAMETROS *param);
int funcao_objetivo(PARAMETROS *param, int *candidato);
int roleta(PARAMETROS *param, INDIVIDUO *pop, int anterior);
double *cria_vetor_double(PARAMETROS *param);
int **crossover(PARAMETROS *param, int *pai, int *mae);
int cmpfunc(const void *a, const void *b);
void mutacao(PARAMETROS *param, int *candidato);
void destruir_populacao(PARAMETROS *param, INDIVIDUO *pop, int tam);
int **crossover_pmx(PARAMETROS *param, int *pai, int *mae);

#endif // GA_H_INCLUDED
