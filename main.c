#include <stdio.h>
#include <stdlib.h>
#include "parametros.h"
#include "ga.h"

void testa_parametros(PARAMETROS *param);

int main(int argc, char *argv[]){
    PARAMETROS *param = NULL;
    int *best = NULL;

    if(argc < 2){
        printf("Argumentos invalidos! \n<instancia> [<n_iteracoes>] [<taxa_mutacao>] [<taxa_crossover>] [<tam_populacao>]\n");
        system("pause");
        exit(1);
    }

    param = inicializa_parametros(argv[1]);

    if(argc >= 6){
        param->tam_pop = atoi(argv[5]);
    }else{
        param->tam_pop = POPULACAO;
    }
    if(argc >= 5){
        param->taxa_crossover = atof(argv[4]);
    }else{
        param->taxa_crossover = CROSSOVER;
    }
    if(argc >= 4){
        param->taxa_mutacao = atof(argv[3]);
    }else{
        param->taxa_mutacao = MUTACAO;
    }
    if(argc >= 3){
        param->numero_iteracoes = atoi(argv[2]);
    }else{
        param->numero_iteracoes = GERACAO;
    }

    testa_parametros(param); // OK

    system("pause");

    best = ga(param);

    destroi_parametros(param);

    return 0;
}

void testa_parametros(PARAMETROS *param){
    int i, j;

    printf("Qtd carros: %d\n", param->qtdCarros);
    printf("Qtd ferrys: %d\n", param->qtdFerrys);
    printf("Satisfacao carro:\n");
    for(i = 0; i < param->qtdCarros; i++){
        printf("%d ", param->satisfacaoCarro[i]);
    }
    printf("\nPeso carros:\n");
    for(i = 0; i < param->qtdFerrys; i++){
        for(j = 0; j < param->qtdCarros; j++){
            printf("%d ", param->pesoCarro[i][j]);
        }
        printf("\n");
    }
    printf("\nCapacidade ferry:\n");
    for(i = 0; i < param->qtdFerrys; i++){
        printf("%d ", param->capacidadeFerry[i]);
    }
    printf("\nTempo chegada carro:\n");
    for(i = 0; i < param->qtdCarros; i++){
        printf("%d ", param->tempoChegadaCarro[i]);
    }
    printf("\nTempo saida ferry:\n");
    for(i = 0; i < param->qtdFerrys; i++){
        printf("%d ", param->tempoSaidaFerry[i]);
    }
    printf("\nOtimo: %d\n", param->otimo);
    printf("Taxa mutacao: %.2f\n", param->taxa_mutacao);
    printf("Taxa crossover: %.2f\n", param->taxa_crossover);
    printf("Tamanho populacao: %d\n", param->tam_pop);
    printf("Numero iteracoes: %d\n", param->numero_iteracoes);
}

