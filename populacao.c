/*  Constroi populacao  */

typedef struct _individuo_{
	int *cromossomo;
	float fitness;
}INDIVIDUO;


void constroi_populacao(PARAMETROS *param, int tamPop){
	INDIVIDUO *pop = NULL;
	
	int i, j;
	
	pop = (INDIVIDUO *)malloc(tamPop * sizeof(INDIVIDUO));
	
	if(pop == NULL){
		erro();
	}
	
	pop->cromossomo = (int *)malloc(param->qtdItens * sizeof(int));
	
	if(pop->cromossomo == NULL){
		erro();
	}
	
	srand(time(NULL));
	
	for(i = 0; i < tamPop; i++){
		for(j = 0; j < param->qtdItens; j++){
			pop[i]->cromossomo[j] = rand() % param->qtdMochilas + 1;
		}
	}
	
}

void capacidade_maxima(PARAMETROS *param, INDIVIDUO *pop, int tamPop){
	int i, j;
	
	float *capacidades_acumuladas;
	
	capacidades_acumuladas = (float *)malloc(param->qtdMochilas * sizeof(float));
	
	for(i = 0; i < tamPop; i++){
		for(j = 0; j < param->qtdMochilas; j++)
			capacidades_acumuladas[j] = 0.f;
		for(j = 0; j < param->qtdItens; j++)
			capacidades_acumuladas[pop->cromossomo[j]] += param->pesoItens[pop->cromossomo[j]][j];
		for(j = 0; j < param->qtdMochilas; j++){
			if(capacidades_acumuladas[j] > param->capacidadeMochilas[j]){
				pop[i]->fitness *= -1;
			}
		}
	}
}

void funcao_objetivo(PARAMETROS *param, INDIVIDUO *individuo){
	int i;
	
	for(i = 0; i < param->qtdItens; i++){
		individuo->fitness += (individuo->cromossomo[i] == 0 ? 0 : param->valorItem[i]);
	}
	
}

void selecao_roleta(PARAMETROS *param, INDIVIDUO *pop, int tamPop){
	int i;
	double escolhido, *fracao, *escala;
	float soma;
	
	srand(time(NULL));
	
	fracao = aloca_memoria(tamPop);
	escala = aloca_memoria(tamPop);
	
	for(i = 0; i < tamPop; i++){
		soma += pop[i]->fitness;
	}
	
	for(i = 0; i < tamPop; i++){
		fracao = (double)pop[i]->fitness / soma;
	}
	
	escala[0] = fracao[0];
	for(i = 1; i < tamPop; i++){
		escala[i] = escala[i-1] + fracao[i];
	}
	
	escolhido = rand() % 1;
	
	i = 0;
	while(escala[i] <= escolhido) i++;
	
	free(escala);
	free(fracao);
	
	return i;	
}

double *aloca_memoria(int tamPop){
	double *vetor = NULL;
	
	vetor = (double *)malloc(tamPop * sizeof(double));
	
	if(vetor == NULL){
		erro();
	}
	
	return vetor;	
}

void crossover(INDIVIDUO *pop, int pai, int mae){
	int i, j, corte;
	int *filho_1, *filho_2;
	
	srand(time(NULL));
	
	filho_1 = aloca_filho(param->qtdItens);
	filho_2 = aloca_filho(param->qtdItens);
	
	corte = rand() % param->qtdItens;
	
}

int *aloca_filho(int tam){
	int *vetor = NULL;
	
	vetor = (int *)malloc(tamPop * sizeof(int));
	
	if(vetor == NULL){
		erro();
	}
	
	return vetor;
}

